﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Каких элементов больше + или - ?
            int[] array = new int[] {-4, -1, 3, 10, 15};
            int negative = 0;
            int positive = 0;
            for(int i=0; i<array.Length;i++)
            {
                if (array[i] < 0)
                    negative++;
                else positive++;
            }
            if (negative > positive)
                Console.WriteLine("Отрицательных элементов больше");
            if (negative < positive)
                Console.WriteLine("Положительных элементов больше");
            if (negative==positive)
                Console.WriteLine("Равное количество");
        }
    }
}
