﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace з._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = Convert.ToInt32(Console.ReadLine());
            int y = Convert.ToInt32(Console.ReadLine());
            double t = (Math.Pow(x, 2)) - (Math.Pow(y, 2));
            if (t >= 0)
                Console.Write("Yes");
            else Console.Write("No");
        }
    }
}
