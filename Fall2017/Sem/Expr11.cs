﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expr._11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("h = ");
            int h = Convert.ToInt32(Console.ReadLine());
            Console.Write("m = ");
            int m = Convert.ToInt32(Console.ReadLine());
            int ang = (Math.Abs(((h + m / 60) * 30) - m * 6)); // угол межу часовой и минутной
            if (ang > 180)
                ang = 360 - ang;
            Console.WriteLine(ang);
        }
    }
}
