﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expr_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            if (a > b) //9, 3
            {
                a = a - b; //6
                b = a + b; //9
                a = b - a; //3
            }
            if (b > a) //10, 8
            {
                b = b - a; //2
                a = b + a; //10
                b = a - b; //8
            }
            Console.WriteLine("a=" + a + " b=" + b);
            Console.ReadKey();
        }
    }
}
