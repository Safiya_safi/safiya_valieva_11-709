﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("n = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("x = ");
            int x= int.Parse(Console.ReadLine());
            Console.Write("y = ");
            int y= int.Parse(Console.ReadLine());
            int k = ((n - 1) / y) + ((n - 1) / x) - ((n - 1) / (y * x));
            Console.WriteLine(k);
            Console.ReadKey();
        }
    }
}
