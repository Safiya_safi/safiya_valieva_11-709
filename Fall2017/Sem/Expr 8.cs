﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8
{
    class Program
    {
        static void Main(string[] args)
        {
            double x1 = double.Parse(Console.ReadLine());
            double x2 = double.Parse(Console.ReadLine());
            double x3 = double.Parse(Console.ReadLine());
            double y1 = double.Parse(Console.ReadLine());
            double y2 = double.Parse(Console.ReadLine());
            double y3 = double.Parse(Console.ReadLine());
            double a = (y2 - y1) / (x2 - x1);
            double b = y1 - a * x1;
            double c = -1 / a;
            double d = y3 - c * x3;
            double x = (d - b) / (a - c);
            double y = a * x + b;
            Console.WriteLine("x= "+x);
            Console.WriteLine("y= "+y);
            Console.ReadKey();
        }
    }
}
