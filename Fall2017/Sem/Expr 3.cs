﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            int h, g=0;
            h = Convert.ToInt32(Console.ReadLine());
            if (0 <= h && h < 6)
                g = h * 30;
            if (6 <= h && h < 12)
                g = (12 - h) * 30;
            if (12 <= h && h < 18)
                g = (h - 12) * 30;
            if (18 <= h && h <= 24)
                g = (24 - h) * 30;
            Console.WriteLine(g);
            Console.ReadKey();
        }
    }
}
